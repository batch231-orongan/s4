console.log("Hello World!");

class Student {
	constructor(name, email, grades){
		this.name = name;
		this.email = email;

		// check first if the array as 4 elements
		if(grades.length === 4){
			if(grades.every(grade => grade >= 0 && grade <= 100)){
					this.grades = grades;
			} else{
				this.grades = undefined;	
			}
		}else{
			this.grades = undefined;
		}

		this.gradeAve = undefined;
		this.passed = undefined;
		this.passedWithHonors= undefined;
		
	}
	
	login(){
		console.log(`${this.email} has logged in`);
		return this;
	}

	logout(email){
		console.log(`${this.email} has logged out`);
		return this;
	}

	listGrades(grades){
		this.grades.forEach(grade => {
			console.log(`${this.name}'s quarterly grade averages are: ${grade}`)
		});
		return this;	
	}

	computeAve(){
		let sum = 0;
		this.grades.forEach(grade => sum = sum + grade);
		this.gradeAve =  sum / 4;
		return this;
	}

	willPass(){
		this.passed = this.computeAve().gradeAve >= 85 ? true : false;
		return this;
	}

	willPassWithHonors(){
		if(this.passed){
			if(this.gradeAve >= 90){
				this.passedWithHonors = true;
			} else{
				this.passedWithHonors = false;
			}
		}else{
			this.passedWithHonors = false;
		}
		return this;
	}

}

class Section {
	constructor(name){
		// every instantiated section object will be have an empty array for its students
		this.name = name;
		this.students = [];
		this.honorStudents = undefined;
		this.honorsPercentage = undefined;
	}

	// method for adding student to this section.
	// this take in the same arguments needed to instantiate a Student Object.
	addStudent(name, email, grades){
		// a Student object will be instantiated before being pushed to the students property
		this.students.push(new Student(name, email, grades));
		// return the Section object afterwards, allowing us to chain this method.
		return this;
	}
	// method for computing how many students in the section are honor students
	countHonorStudent(){
		let count = 0;
		// remember that each student here is an instatiated from the Student Class
		this.students.forEach(student => {
			if(student.computeAve().willPass().willPassWithHonors().passedWithHonors){
				count++;
			}
		})
		this.honorStudents = count;
		return this;
	}

	computeHonorsPercentage(){
		this.honorsPercentage = (this.honorStudents / this.students.length)*100;
		return this;
	}
}
// instantiate a Section object
// const section1A = new Section("section1A");
// console.log(section1A);

/*
	Mini-Activity: (15 mins)
    Define a Grade class whose constructor will accept a number argument to serve as its grade level. It will have the following properties:


    1. level initialized to passed in number argument
    2. sections initialized to an empty array
    3. totalStudents initialized to zero
    4. totalHonorStudents initialized to zero
    5. batchAveGrade set to undefined
    6. batchMinGrade set to undefined
    7. batchMaxGrade set to undefined
    
    Create a new instance for Class Grade
    Please send your output in Hangouts

*/

class Grade {
	constructor(level){
		this.level = level;
		this.sections = [];
		this.totalStudents = 0;
		this.totalHonorStudents = 0;
		this.batchAveGrade = undefined;
		this.batchMinGrade = undefined;
		this.batchMaxGrade = undefined;
	}

	addSection(name){
		this.sections.push(new Section(name));
		return this;
	}

	countStudents(){
		this.sections.forEach(section => this.totalStudents = this.totalStudents + section.students.length);
		return this;
	}

	countHonorStudents(){
		let count = 0;
		this.sections.forEach(section => {
			section.students.forEach(student =>{
				if(student.computeAve().willPass().willPassWithHonors().passedWithHonors){
					count++
				}
			})
		})
		this.totalHonorStudents =count;
		return this;
	}

	/*
		Activity #4:
	Function Coding Activity: (1 hr)

	1. Define a computeBatchAve() method that will get the average of all the students' grade averages and divide it by the total number of students in the grade level. The batchAveGrade property will be updated with the result of this operation.

	2. Define a method named getBatchMinGrade() that will update the batchMinGrade property with the lowest grade scored by a student of this grade level regardless of section.

	3. Define a method named getBatchMaxGrade() that will update the batchMaxGrade property with the highest grade scored by a student of this grade level regardless of section.

	4. Send your output screenshots in Hangouts and link it in Boodle.	
	*/
	computeBatchAve(){
		// let sectionAve = 0;

		// this.sections.forEach(section => {
		// 	section.students.forEach(student => sectionAve = sectionAve + student.computeAve() )
		// })
		// this.batchAveGrade = sectionAve / this.sections.length;

		let sum = 0;
		let studentCount = 0;

		this.sections.forEach(section => {
			section.students.forEach(student => {
				sum = sum + student.computeAve().gradeAve;
				studentCount++;
			})
		})

		this.batchAveGrade = Math.round(sum / studentCount);
		return this;
	}

	getBatchMinGrade(){
		// let sectionMin = Object.values(this.sections).map(Object.values(this.sections).map({grades}) => grades);

		// console.log(sectionMin);

		// this.batchMinGrade = Math.min(...sectionMin);

		let min;
		this.sections.forEach(section => {
			section.students.forEach(student => {
				student.grades.forEach(grade => {
					if(typeof min === 'undefined' || grade < min){
						min = grade;
					}
				})
			})
		})

		this.batchMinGrade = min;
		return this;
	}

	getBatchMaxGrade(){
		// this.batchMaxGrade = this.section.forEach()
		let max;
		this.sections.forEach(section => {
			section.students.forEach(student => {
				student.grades.forEach(grade => {
					if(typeof max === 'undefined' || grade > max){
						max = grade;
					}
				})
			})
		})

		this.batchMaxGrade = max;
		return this;
	}
}

// Instantiate Grade

const grade1 = new Grade(1);
console.log(grade1);

// populate the grade level with sections
grade1.addSection("section1A");
grade1.addSection("section1B");
grade1.addSection("section1C");
grade1.addSection("section1D");

const section1A = grade1.sections.find(section => section.name === "section1A");
const section1B = grade1.sections.find(section => section.name === "section1B");
const section1C = grade1.sections.find(section => section.name === "section1C");
const section1D = grade1.sections.find(section => section.name === "section1D");

// papulate the sections with students
section1A.addStudent("Tony", "starksindustries@mail.com", [89, 84, 78, 88]);
section1A.addStudent("Peter", "spideyman@mail.com", [78, 82, 79, 85]);
section1A.addStudent("Wanda", "scarlettMaximoff@mail.com", [87, 89, 91, 93]);
section1A.addStudent("Steve", "captainRogers@mail.com", [91, 89, 92, 93]);

section1B.addStudent('Jeremy', 'jeremy@mail.com', [85, 82, 83, 89]);
section1B.addStudent('Johnny', 'johnny@mail.com', [82, 86, 77, 88]);
section1B.addStudent('Jerome', 'jerome@mail.com', [89, 85, 92, 91]);
section1B.addStudent('Janine', 'janine@mail.com', [90, 87, 94, 91]);

section1C.addStudent('Faith', 'faith@mail.com', [87, 85, 88, 91]);
section1C.addStudent('Hope', 'hope@mail.com', [85, 87, 84, 89]);
section1C.addStudent('Love', 'love@mail.com', [91, 87, 90, 88]);
section1C.addStudent('Joy', 'joy@mail.com', [92, 86, 90, 89]);

section1D.addStudent('Eddie', 'eddie@mail.com', [85, 87, 86, 92]);
section1D.addStudent('Ellen', 'ellen@mail.com', [88, 84, 86, 90]);
section1D.addStudent('Edgar', 'edgar@mail.com', [90, 89, 92, 86]);
section1D.addStudent('Eileen', 'eileen@mail.com', [90, 88, 93, 84]);

/*
	ALTERNATIVE ACTIVITY:

	/*
    Instructions:
    Given the Artist and Songs classes, create the following methods:
    Artist:
        1. Get the average ratings using getAveRatings() method of each Artist.
        2. Create a getAward() method, if the ratings goes up to 8 and above return true otherwise false
        3. Make the methods chainable

    Songs:
        1. Create addArtist() method. The artists should be instantiated in the method together with the name of the artist and their ratings as its arguments
        2. Get the number of award winning artists with countAwardWinningArtists() method in each song

    Push in your gitlab account and link in s4


*/

class Artist {
    constructor(name, ratings){
        this.name = name;

        if(ratings.length >= 5){
            this.ratings = ratings;
        } else {
            this.ratings = "unknown artist"
        }

        this.aveRatings = undefined;
        this.awardWinning = undefined;
    }

    getAveRating(){
    	let sum = 0;
    	let ratingCount = 0;
    	this.ratings.forEach(rating => {
    		sum = sum + rating;
    		ratingCount++;
    	});

    	this.aveRatings = (sum /ratingCount).toFixed(2);
    	return this;
    }

    getAward(){
    	this.awardWinning = this.getAveRating().aveRatings >= 8 ? true : false;
    	return this;

    }

    
}



class Songs {
    constructor(title){
        this.title = title;
        this.artists = [];
        this.awardWinningArtists = undefined;
    }

    addArtist(name, ratings){
    	this.artists.push(new Artist(name, ratings));
    	return this;
    }

    countAwardWinningArtists(){
    	let count = 0;
    	this.artists.forEach(artist => {
    		if(artist.getAward().awardWinning){
    			count++
    		}
    	})
    	this.awardWinningArtists = count;
    	return this;
    }
}


// instance for testing
let song1 = new Songs("Black Swan");
song1.addArtist("BTS", [10, 10, 9, 10, 8, 10, 8]);
song1.addArtist("Suga", [9, 8, 10, 7, 10, 10, 8]);
song1.addArtist("UA", [7, 5, 6, 10, 2, 3, 4]);
song1.addArtist("BrandX", [6, 5, 6, 7, 4, 5, 4]);


